package br.com.cadpessoa.api;


import br.com.cadpessoa.domain.exception.PessoaException;
import br.com.cadpessoa.dto.CadastroPessoaDTO;
import br.com.cadpessoa.service.PessoaService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
@RequestMapping(value = "/pessoa")
public class PessoaController {

    static Logger logger = Logger.getLogger(PessoaController.class.getName());

    @Autowired
    PessoaService pessoaService;

    @ApiOperation(value = "Salva uma nova pessoa")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Pessoa salva com sucesso."),
            @ApiResponse(code = 400, message = "Body não é valido"),
            @ApiResponse(code = 500, message = "Ocorreu algum erro por parte do servidor.")

    })
    @PostMapping()
    public ResponseEntity<?> cadastrarPessoa(@RequestBody CadastroPessoaDTO cadastroPessoaDTO) {
        try {
            pessoaService.cadastrarPessoas(cadastroPessoaDTO);
            logger.log(Level.INFO, "Pessoa cadastrada com sucesso.");
            return ResponseEntity.status(HttpStatus.CREATED).body(cadastroPessoaDTO);

        } catch (PessoaException e) {
            logger.log(Level.INFO, e.getMessage());
            return ResponseEntity.badRequest()
                    .body(e.getMessage());
        } catch (
                Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
            return ResponseEntity.internalServerError()
                    .body(e.getMessage());
        }
    }

    @ApiOperation(value = "Lista as pessoas cadastradas")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Pessoa salva com sucesso."),
            @ApiResponse(code = 500, message = "Ocorreu algum erro por parte do servidor.")

    })
    @GetMapping()
    public ResponseEntity<?> listarPessoas() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(pessoaService.listarPessoas());
        } catch (
                Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
            return ResponseEntity.internalServerError()
                    .body(e.getMessage());
        }
    }


}



