package br.com.cadpessoa.service;


import br.com.cadpessoa.domain.enums.TipoIdentificadorPessoaEnum;
import br.com.cadpessoa.domain.exception.PessoaException;
import br.com.cadpessoa.domain.model.Pessoa;
import br.com.cadpessoa.domain.repository.PessoaRepository;
import br.com.cadpessoa.dto.CadastroPessoaDTO;
import br.com.cadpessoa.dto.ConsultaPessoaDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PessoaService {

    @Autowired
    PessoaRepository repository;

    public void cadastrarPessoas(CadastroPessoaDTO cadastroPessoaDTO) throws PessoaException {
        validarIdentificador(cadastroPessoaDTO);
        Pessoa pessoa = new Pessoa();
        BeanUtils.copyProperties(cadastroPessoaDTO, pessoa);
        pessoa.setTipoIdentificadorPessoa(cadastroPessoaDTO.getIdentificadorPessoa().length() == 11 ? TipoIdentificadorPessoaEnum.FISICA : TipoIdentificadorPessoaEnum.JURIDICA);
        repository.save(pessoa);
    }

    public List<ConsultaPessoaDTO> listarPessoas() {
        ArrayList<ConsultaPessoaDTO> pessoasList = new ArrayList<>();
        for (Pessoa pessoa : repository.findAll()) {
            ConsultaPessoaDTO consultaPessoaDTO = new ConsultaPessoaDTO();
            BeanUtils.copyProperties(pessoa, consultaPessoaDTO);
            pessoasList.add(consultaPessoaDTO);
        }
        return pessoasList;
    }

    private void validarIdentificador(CadastroPessoaDTO cadastroPessoaDTO) throws PessoaException {
        if (cadastroPessoaDTO.getIdentificadorPessoa() == null || cadastroPessoaDTO.getIdentificadorPessoa().length() < 11
                || cadastroPessoaDTO.getIdentificadorPessoa().length() > 14) {
            throw new PessoaException("Identificador da pessoa inválido");
        }
    }
}
