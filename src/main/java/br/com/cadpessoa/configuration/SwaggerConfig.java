package br.com.cadpessoa.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Value("${application.version}")
    private String version;

    @Configuration
    public class SpringFoxConfig {
        @Bean
        public Docket api() {
            return new Docket(DocumentationType.SWAGGER_2)
                    .apiInfo(
                            new ApiInfoBuilder()
                                    .version(version)
                                    .title("Cadastro de Pessoas Rest API")
                                    .description("Documentação da api de cadastro de pessoas").build()
                    )
                    .select()
                    .apis(RequestHandlerSelectors.basePackage("br.com.cadpessoa"))
                    .paths(PathSelectors.any())
                    .build();
        }
    }
}
