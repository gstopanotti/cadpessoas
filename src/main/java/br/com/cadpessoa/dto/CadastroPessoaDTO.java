package br.com.cadpessoa.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CadastroPessoaDTO {

    private String nome;

    private String identificadorPessoa;


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getIdentificadorPessoa() {
        return identificadorPessoa;
    }

    public void setIdentificadorPessoa(String identificadorPessoa) {
        this.identificadorPessoa = identificadorPessoa;
    }
}
