package br.com.cadpessoa.dto;

import br.com.cadpessoa.domain.enums.TipoIdentificadorPessoaEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsultaPessoaDTO {

    private String nome;

    private TipoIdentificadorPessoaEnum tipoIdentificadorPessoa;

    private String identificadorPessoa;


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public TipoIdentificadorPessoaEnum getTipoIdentificadorPessoa() {
        return tipoIdentificadorPessoa;
    }

    public void setTipoIdentificadorPessoa(TipoIdentificadorPessoaEnum tipoIdentificadorPessoa) {
        this.tipoIdentificadorPessoa = tipoIdentificadorPessoa;
    }

    public String getIdentificadorPessoa() {
        return identificadorPessoa;
    }

    public void setIdentificadorPessoa(String identificadorPessoa) {
        this.identificadorPessoa = identificadorPessoa;
    }
}
