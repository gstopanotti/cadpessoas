package br.com.cadpessoa.domain.exception;

public class PessoaException extends Exception{

    public PessoaException(String message) {
        super(message);
    }
}
