package br.com.cadpessoa.domain.enums;

public enum TipoIdentificadorPessoaEnum {

    FISICA("CPF"),
    JURIDICA("CNPJ");


    private String descricao;

    TipoIdentificadorPessoaEnum(String descricao) {
        this.descricao = descricao;
    }


    public String getDescricao() {
        return descricao;
    }
}
