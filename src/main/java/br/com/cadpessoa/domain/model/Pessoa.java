package br.com.cadpessoa.domain.model;

import br.com.cadpessoa.domain.enums.TipoIdentificadorPessoaEnum;
import com.sun.istack.NotNull;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Pessoa {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idPessoa;
    @NotNull
    private String nome;
    @NotNull
    private TipoIdentificadorPessoaEnum tipoIdentificadorPessoa;
    @NotNull
    private String identificadorPessoa;


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public TipoIdentificadorPessoaEnum getTipoIdentificadorPessoa() {
        return tipoIdentificadorPessoa;
    }

    public void setTipoIdentificadorPessoa(TipoIdentificadorPessoaEnum tipoIdentificadorPessoa) {
        this.tipoIdentificadorPessoa = tipoIdentificadorPessoa;
    }

    public String getIdentificadorPessoa() {
        return identificadorPessoa;
    }

    public void setIdentificadorPessoa(String identificadorPessoa) {
        this.identificadorPessoa = identificadorPessoa;
    }
}
